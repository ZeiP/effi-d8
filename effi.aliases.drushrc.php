<?php
// Link this file to ~/.drush

$aliases['docker'] = array(
  'uri' => 'localhost:8089',
  'remote-host' => 'docker',
  'remote-user' => 'root',
  'ssh-options' => '-p 2229 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no',
  'root' => '/wwwroot/current',
  'path-aliases' => array(
//    '%files' => '/wwwroot/current/sites/default/files',
    '%dump-dir' => '/tmp',
  ),
  'command-specific' => array(
    'sql-sync' => array(
      'sanitize' => TRUE,
      'no-cache' => TRUE,
      'no-ordered-dump' => TRUE,
      'structure-tables' => array(
        'custom' => array(
          'cache',
          'cache_filter',
          'cache_menu',
          'cache_page',
          'cache_views_data',
          'history',
          'sessions',
        ),
      ),
    ),
  ),
);
